package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseRepository {

    Map<Integer,WareHouse> wareHouseIdToObjectMap = new HashMap<Integer,WareHouse>();
    Map<Integer,Product> productIdToObjectMap = new HashMap<Integer,Product>();
    Map<Integer,Map<Integer,Integer>> stockMap = new HashMap<Integer,Map<Integer,Integer>>();
    Map<Integer,Order> orderToObjectMap = new HashMap<Integer,Order>();
    Map<Integer,OrderWithWarehouse> orderWithWarehouseMap = new HashMap<>();

    public void insertWareHouseObject(WareHouse w) {
        // TODO: Add this to a Database Table WareHouse :- {workhouse_id, name, address, lat, lng}
        wareHouseIdToObjectMap.put(w.getWareHouseId(), w);
    }

    public void insertProduct(Product product) {
        // TODO: Add this to a Database Table Product :- {product_id, product_name, dimension, weight}
        productIdToObjectMap.put(product.getProductId(),product);
    }

    public void assignProductToWarehouse(int wareHouseId, int productId, int quantity) {
        if(!this.wareHouseIdToObjectMap.containsKey(wareHouseId)) {
            System.out.println("The wareHouseId entered is not valid");
            return;
        }
        if(!this.productIdToObjectMap.containsKey(productId)) {
            System.out.println("The productId entered is not valid");
            return;
        }

        // check if there is an entry for this warehouse
        if(stockMap.containsKey(wareHouseId)) {
            // warehouse already exists
            Map<Integer,Integer> productToQauntityMap = stockMap.get(wareHouseId);
            if(productToQauntityMap.containsKey(productId)){
                // product already exists in the warehouse so change its quantity
                int existingQuantity = productToQauntityMap.get(productId);
                int newQuantity = existingQuantity + quantity;
                productToQauntityMap.put(productId,newQuantity);
                System.out.println("changing quantity from  " + existingQuantity + "to " + newQuantity);
            } else {
                // new product
                productToQauntityMap.put(productId,quantity);
                System.out.println("adding new quantity " + quantity);
            }
            stockMap.put(wareHouseId,productToQauntityMap);
        } else {
            // new warehouse
            Map<Integer,Integer> productToQauntityMap = new HashMap<Integer,Integer>();
            productToQauntityMap.put(productId,quantity);
            System.out.println("adding new quantity " + quantity);
            stockMap.put(wareHouseId,productToQauntityMap);
        }
    }

    public void storeOrder(Order order) {
        // TODO: Add this to a Database Table Order :- {order_id, product_id, dimension, weight}
        orderToObjectMap.put(order.getOrderId(),order);
    }

    public void assignWareHouse(int orderId) {
        Order order = orderToObjectMap.get(orderId);
        List<Integer> productsInOrder = new ArrayList(order.getProductIdQuantityMap().keySet());

        // will map product and all the warehouse with that product
        Map<Integer,List<OrderStock>> mapOfProductIdToListOfOrderStock = new HashMap<>();
        // get warehouse stock for every product in order
        for(int productId : productsInOrder) {
            List<OrderStock> listOfOrderStock = new ArrayList<OrderStock>();
            for(Map.Entry<Integer,Map<Integer,Integer>> stockEntry : stockMap.entrySet()) {
                int wareHouseId = stockEntry.getKey();
                Map<Integer,Integer> productToStockMap = stockEntry.getValue();
                if(productToStockMap.containsKey(productId)) {
                    WareHouse wareHouse = wareHouseIdToObjectMap.get(wareHouseId);
                    int available = productToStockMap.get(productId);
                    int required = order.getProductIdQuantityMap().get(productId);
                    double distance = calculateDistance(order.getLat(),order.getLng(),wareHouse.getLat(),wareHouse.getLng());
                    OrderStock os = new OrderStock(orderId,wareHouseId,productId,available,required,distance);
                    listOfOrderStock.add(os);
                }
            }
            // will make the list of warehouse to be in ascending order of distance from order location
            Collections.sort(listOfOrderStock);
            mapOfProductIdToListOfOrderStock.put(productId,listOfOrderStock);
        }

       // assign warehouse to order for every product in order
        for(Map.Entry<Integer,List<OrderStock>> entry : mapOfProductIdToListOfOrderStock.entrySet()) {
            int productId = entry.getKey();
            List<OrderStock> orderStockList = entry.getValue();
            int required = orderStockList.get(0).getRequired();
            for(OrderStock orderStock : orderStockList) {
                int available = orderStock.getAvailable();
                if(required <= available){
                    // we got all required from this wareHouse
                    OrderWithWarehouse orderWithWarehouse = new OrderWithWarehouse(orderStock,required);
                    orderWithWarehouseMap.put(orderStock.getOrderId(),orderWithWarehouse);
                    updateStockMap(orderWithWarehouse);
                    break;
                } else {
                    // take all from this warehouse
                    OrderWithWarehouse orderWithWarehouse = new OrderWithWarehouse(orderStock,available);
                    orderWithWarehouseMap.put(orderStock.getOrderId(),orderWithWarehouse);
                    updateStockMap(orderWithWarehouse);
                }
            }
        }

        // print assigned order
        for(Map.Entry<Integer,OrderWithWarehouse> entry : orderWithWarehouseMap.entrySet()) {
            OrderWithWarehouse orderWithWarehouse = entry.getValue();
            orderWithWarehouse.print();
        }
    }

    private void updateStockMap(OrderWithWarehouse orderWithWarehouse) {
        Map<Integer,Integer> productToStockMapInWarehouse = stockMap.get(orderWithWarehouse.getWarehouseId());
        int available = productToStockMapInWarehouse.get(orderWithWarehouse.getProductId());
        available = available - orderWithWarehouse.getQuantityToBeOrdered();
        if(available == 0) {
            // no more available stock for this product in warehouse so remove this product
            productToStockMapInWarehouse.remove(orderWithWarehouse.getProductId());
        } else {
            productToStockMapInWarehouse.put(orderWithWarehouse.getProductId(),available);
        }
        stockMap.put(orderWithWarehouse.getWarehouseId(),productToStockMapInWarehouse);
    }

    /**
     * distnace calculation based on Haversine_formula
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    public double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;

        return dist;
    }
}
