package com.company;

public class OrderStock implements Comparable<OrderStock> {
    private int orderId;
    private int wareHouseId;
    private int productId;
    private int available;
    private int required;
    private double distance;

    public OrderStock(int orderId, int wareHouseId, int productId, int available, int required, double distance) {
        this.orderId = orderId;
        this.wareHouseId = wareHouseId;
        this.productId = productId;
        this.available = available;
        this.required = required;
        this.distance = distance;
    }

    @Override
    public int compareTo(OrderStock orderStock){
        return Double.compare(this.getDistance(),orderStock.getDistance());
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(int wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
