package com.company;

import java.util.Map;

public class Order {
    private static int counter = 0;
    private int orderId;
    private Map<Integer, Integer> productIdQuantityMap;
    private String shipAddress;
    private Double lat;
    private Double lng;

    public Order(Map<Integer, Integer> productIdQuantityMap, String shipAddress, Double lat, Double lng) {
        this.orderId = ++counter;
        this.productIdQuantityMap = productIdQuantityMap;
        this.shipAddress = shipAddress;
        this.lat = lat;
        this.lng = lng;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Map<Integer, Integer> getProductIdQuantityMap() {
        return productIdQuantityMap;
    }

    public void setProductIdQuantityMap(Map<Integer, Integer> productIdQuantityMap) {
        this.productIdQuantityMap = productIdQuantityMap;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
