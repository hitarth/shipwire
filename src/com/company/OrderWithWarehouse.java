package com.company;

public class OrderWithWarehouse {
    private int orderId;
    private int warehouseId;
    private int productId;
    private int quantityToBeOrdered;

    public OrderWithWarehouse(int orderId, int warehouseId, int productId, int quantityToBeOrdered) {
        this.orderId = orderId;
        this.warehouseId = warehouseId;
        this.productId = productId;
        this.quantityToBeOrdered = quantityToBeOrdered;
    }

    public OrderWithWarehouse(OrderStock orderStock, int quantity) {
        this.orderId = orderStock.getOrderId();
        this.warehouseId = orderStock.getWareHouseId();
        this.productId = orderStock.getProductId();
        this.quantityToBeOrdered = quantity;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantityToBeOrdered() {
        return quantityToBeOrdered;
    }

    public void setQuantityToBeOrdered(int quantityToBeOrdered) {
        this.quantityToBeOrdered = quantityToBeOrdered;
    }

    public void print() {
        System.out.println("OrderId = " + this.getOrderId() + " , " + "ProductId = " + this.getProductId() + " , " +
                "wareHouseId = " +  this.getWarehouseId() + "," + "QuantityToBeOrdered = " + this.getQuantityToBeOrdered());

    }
}
