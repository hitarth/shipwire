package com.company;

public class Product {
    private static int counter = 0;
    private int productId;;
    private String productName;
    private String dimension;
    private double weight;

    public Product(String productName, String dimension, double weight) {
        this.productId = ++counter;
        this.productName = productName;
        this.dimension = dimension;
        this.weight = weight;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
