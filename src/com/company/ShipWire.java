package com.company;

import java.io.Console;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

public class ShipWire {

    public static final String usage = " \n\n Shipwire Order fulfillment Software :- \n" +
            "Following are different options which can be entered \n" +
            "1. {Option=WareHouseEntry}{Name=Boston}{Address=1610 Worcester Road, Framingham, MA-01702} \n" +
            "2. {Option=ProductEntry}{Name=Book}{Dimension=dimension}{Weight=1.2} \n" +
            "3. {Option=AssignProductToWarehouse}{WareHouseId=1}{ProductId=2}{Quantity=3} \n" +
            "4. {Option=CreateOrder}{ProductWithQuantity=1:1,2:1,3:2}{ShipAddress=1610 Worcester Road, Framingham, MA-01702} \n" +
            "5. {Option=AssignWareHouse}{OrderId=1} \n" +
            "6. Exit \n";


    public static void main(String[] args) throws IOException {
        String option = null;
        DatabaseRepository databaseRepository = new DatabaseRepository();
        while(option == null || !option.equals("Exit") ){
            Console console = System.console();
            option = console.readLine(usage);
            if(option.equals("Exit")) {
                break;
            }
            // Since cannot split on space using braces to separate different arguments
            String[] arguments = option.split("(?<=})");
            String function = parseFormattedArgument(arguments[0]);
            switch (function) {
                case "WareHouseEntry":
                    String wareHouseName = parseFormattedArgument(arguments[1]);
                    String address = parseFormattedArgument(arguments[2]);
                    LatLng latLng = null;
                    try {
                        latLng = AddressConverter.convertToLatLong(address);
                    } catch (JSONException e) {
                        System.out.println("Could Not convert address to Lat Lng");
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    WareHouse wareHouse = new WareHouse(wareHouseName,address, latLng.getLat(), latLng.getLng());
                    databaseRepository.insertWareHouseObject(wareHouse);
                    System.out.println("Successfully added a new WareHouse with id " + wareHouse.getWareHouseId());
                    break;
                case "ProductEntry":
                    System.out.println(option);
                    String productName = parseFormattedArgument(arguments[1]);
                    String dimension = parseFormattedArgument(arguments[2]);
                    double weight = Double.parseDouble(parseFormattedArgument(arguments[3]));
                    Product product = new Product(productName,dimension,weight);
                    databaseRepository.insertProduct(product);
                    System.out.println("Successfully added a new Product with id " + product.getProductId());
                    break;
                case "AssignProductToWarehouse":
                    int wareHouseId = Integer.parseInt(parseFormattedArgument(arguments[1]));
                    int productId = Integer.parseInt(parseFormattedArgument(arguments[2]));
                    int quantity = Integer.parseInt(parseFormattedArgument(arguments[3]));
                    databaseRepository.assignProductToWarehouse(wareHouseId,productId,quantity);
                    break;
                case "CreateOrder":
                    String productWithQuantity = parseFormattedArgument(arguments[1]);
                    String shipAddress = parseFormattedArgument(arguments[2]);
                    Map<Integer,Integer> productIdQuantityMap = getProductWithQuantityMapping(productWithQuantity);
                    LatLng orderLatLng = null;
                    try {
                        orderLatLng = AddressConverter.convertToLatLong(shipAddress);
                    } catch (JSONException e) {
                        System.out.println("Could Not convert address to Lat Lng");
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    Order order = new Order(productIdQuantityMap, shipAddress, orderLatLng.getLat(),orderLatLng.getLng());
                    databaseRepository.storeOrder(order);
                    System.out.println("Successfully added a new Order with id " + order.getOrderId());
                    break;
                case "AssignWareHouse":
                    int orderId = Integer.parseInt(parseFormattedArgument(arguments[1]));
                    databaseRepository.assignWareHouse(orderId);
                    break;
                case "Exit":
                    break;
                default:
                    throw new IllegalArgumentException("Invalid Option Passed");
            }
        }
    }

    /**
     * Takes a String like "pid:quantity,pid1:quantity1" and return map of pid to Quantity
     * @param productWithQuantity
     * @return
     */
    private static Map<Integer,Integer> getProductWithQuantityMapping(String productWithQuantity) {
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        String[] individualProductsWithQuantity = productWithQuantity.split(",");
        for(String s:individualProductsWithQuantity) {
            String[] splittedString = s.split(":");
            int productId = Integer.parseInt(splittedString[0]);
            int quantity = Integer.parseInt(splittedString[1]);
            if(map.containsKey(productId)){
                // product already exists so change its quantity
                int existingQuantity = map.get(productId);
                int newQuantity = existingQuantity + quantity;
                map.put(productId,newQuantity);
                System.out.println("changing quantity from  " + existingQuantity + "to " + newQuantity);
            } else {
                // new product
                map.put(productId,quantity);
                System.out.println("adding new quantity " + quantity);
            }
        }
        return map;
    }

    /**
     * This functions takes the formatted argument as input.
     * @param formattedString
     * @return
     */
    public static String parseFormattedArgument(String formattedString) {

        // Trim off the '{' and '}' chars and split based on first occurance of '=' delimiter
        final String[] arguments = formattedString.substring(1, formattedString.length() - 1)
                .split("=",2);
        return arguments[1];
    }
}
