package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class AddressConverter {
    private static final String URL = "https://maps.googleapis.com/maps/api/geocode/json";//?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA"
    private static final String KEY ="AIzaSyCVXz2vE87T_29QZCvkfs-BaD4nvthbMxE";

    public static LatLng convertToLatLong(String fullAddress) throws IOException, JSONException, URISyntaxException {
        System.out.println("Trying to connect to geocode api");
        URL url = new URL(URL + "?address="
                + URLEncoder.encode(fullAddress, "UTF-8") + "&key="+KEY);
        System.out.println(url.toURI().toString());
        // Open the Connection
        URLConnection conn = url.openConnection();

        InputStream in = conn.getInputStream() ;
        System.out.println("Got it");
       try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);

           JSONArray resultArray  = (JSONArray)json.get("results");
           // Assuming the most accurate result would be the first address
           JSONObject resultObject  = (JSONObject)resultArray.get(0);
           JSONObject geometryObject = (JSONObject)resultObject.get("geometry");
           JSONObject locationObject = (JSONObject)geometryObject.get("location");
           double lat = locationObject.getDouble("lat");
           double lng = locationObject.getDouble("lng");
           LatLng latLng = new LatLng(lat, lng);
           return latLng;
        } finally {
            in.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    public static void main(String args[]) throws IOException, JSONException, URISyntaxException {
        AddressConverter a = new AddressConverter();
        convertToLatLong("1600 Amphitheatre Parkway, Mountain View, CA");
    }
}