# README #

This is a command line application developed for shipwire coding exercise

### How do I get set up? ###
go to shipwire/out/artifacts/shipwire_jar/
run the jar named shipwire.jar using following command
java -jar shipwire.jar

There are 6 operations which can be performed using this application. The options need to be in the same format as given below,

1. {Option=WareHouseEntry}{Name=Boston}{Address=1610 Worcester Road, Framingham, MA-01702} 
This option is used to enter a warehouse information in the system. 

2. {Option=ProductEntry}{Name=Book}{Dimension=dimension}{Weight=1.2} 
This option is used to enter a product related information.

3. {Option=AssignProductToWarehouse}{WareHouseId=1}{ProductId=2}{Quantity=3} 
This option indicates that 3 quantities of Product with Product ProductId 2 would be assigned to WareHouse with warehouseId 1.

4. {Option=CreateOrder}{ProductWithQuantity=1:1,2:1,3:2}{ShipAddress=1610 Worcester Road, Framingham, MA-01702} 
This option creates an Order with address as ShipAddress and CSV of Product with Quantity information. 1:1 indicated product with productId 1 and quantity of 1.

5. {Option=AssignWareHouse}{OrderId=1} 
This option will assign warehouse to all the products in the ORder with orderId 1.

6. Exit 
Exit from application